// console.log("hello");

// [Section] Function
	// Function
		// 
		//
		// 

	// Function declaration
		// (function statements) - defines a function with specified parameters.

		/*	Syntax:
				function functionName(){
					code block (statements)
				}
		*/

		// function keyword - used to define a javascript functions
		// functionName - the function name. Functions are named to be able to use later in the code.
		// function block {} - the statements which comprise the body of the function. This is where the code will be executed.

			function printName(){
				console.log("My name is john");
				console.log("My last name is Dela Cruz.");
			}

		// Function Invocation
			// The code block and statements inside a function is not immediately executed when the function is define/declared. The code block and statements inside a function is executed when the function is invoked.
			// It is common to use term "call a function" instead of "invoke a function"

				// Let's invoke the function that we declared.
				printName();

				printName();

// [Sections] Function Declaration and Function Expression
	// function declaration
		// A function can be created through function declaration by using the keyword function and adding function name.

		// Declared function are not executed immediately.

			declaredFunction();

			function declaredFunction(){
				console.log("hello world from declaredFunction");
			}

	// function expression
		// a function can also be stored in a variable. This is called function expression.

		// Anonymous function - function without a name.

		// variableFunction(); - hindi pwede sa una to <----

		let variableFunction = function(){
			console.log("Hello from variableFunction");
		}

		variableFunction();

		let funcExpression = function funcName(){
			console.log("Hello from funcExpression!");
		}

		funcExpression();

	// You can reassign declared functions and function expression to new anonymous function.

		declaredFunction = function(){
			console.log("Updated declaredFunction");
		}

		declaredFunction();

		// reassignment function expression

		funcExpression = function(){
			console.log("Updated funcExpression!");
		}

		funcExpression();

		// Function expression using const keyword
		const constantFunc = function(){
			console.log("Initialized with const!");
		}

		constantFunc();

		// cannot reassign with const
		/*
			constFunc = function(){
				console.log("Cannot be reassigned!")
				}
				constantFunc();
		*/ 
		
// [Section] Function Scoping

/*
	Scope is accessibility of variable w/in our program.

	Javascript Variable, It has 3 types of scope:
	1. local/block scope
	2. global scope
	3. function scope
*/
		// example of local scope
		{
			let localVar = "Armando Perez";
			console.log(localVar);
		}

		// example of global scope
	let globalVar = "Mr. Wordwide";

	// Function Scope
	// Javascript has function scope: Each function creates a new scope.
	// Variables defined inside a function are not accessible outside the function.

	function showNames(){
		let functionLet = "Jane";
		const functionConst = "John";

		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	// The variables. functionLet, functionConst, are function scoped and cannot be accessed outside of the function that they were declared.
	// console.log(functionLet);
	// console.log(functionConst);


// [Section] Nested Functions
	// You can create another function inside a funtion. This is called nested function. 

	function myNewFunction(){
		let name = "Jane";
		console.log(name);
		let nestFunction = function nestedFunction(){
			let nestedName = "John";

			console.log(nestedName);
			console.log(name);
		}

		nestFunction();
	}

	myNewFunction();


	// Function and Global Scoped Variables
		// Global Scope Variable
		let globaName = "Alexandro";

		function myNewFunction2(){
			let nameInside = "Renz";

			console.log(globaName);
			console.log(nameInside);
		}

		myNewFunction2();


// [Section] Using Alert
		// alert(); - it allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log() which only shows on the consoles. It allows us to show a short dialog or instruction to our users. The page will wait until the user dismiss the dialog 

		alert("hello World"); 
		// This will run immediately when page loads.

		// Syntax:
		// alert("<Message in string>");

		function showSampleAlert(){
			alert("Hello, User!");
		}

		showSampleAlert();

		// Notes on use of alert()
			// Show only an alert  (For short dialogs/Messages to the user)
			// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// [Section] prompt();

		// prompt allows us to show a small window at the top of the browser to gather user input. The input from the prompt() will be returned as a String once the user dismisses the window.

		let samplePrompt = prompt("Enter your name: ");
		console.log(samplePrompt);

		/*
			Syntax:
				prompt("<dialogInString>");
		*/

		let sampleNullPrompt = prompt("Dont enter anything.");
		console.log(sampleNullPrompt);
		console.log(typeof sampleNullPrompt);

		// sample
		function printWelcomeMessages(){
			let firstName = prompt("Enter your First Name: ");
			let lastName = prompt("Enter your Last Name: ");

			console.log("Hello," + firstName + " " + lastName + "!");
		}

		printWelcomeMessages();

// [Section] Function Naming Convention

		// Function names should be definitive of the task it will perform. It usually contains a verb.

			function getCourses(){
				let course = ["Science 101", "Math 101", "English 101"];

				console.log(course);
			}

			getCourses();

		// Avoid generic names to avoid confusion within your code/program.
			function getName(){
				let name = "Jamie";
				console.log(name);
			}

			getName();


		// Avoid pointless and inappropriate function names.
			function foo(){
				console.log(25%5);
			}

			foo();


		// name your function in small caps. Follow camelCasing when naming variables and functions.
			function displayCarInfo(){
				console.log("Brand: Toyota");
				console.log("Type: Sedan");
				console.log("Price: 1,500,000");
			}

			displayCarInfo();